package stockmarket;

public class Trade 
{
	String buyer;
	String seller;
	Integer buyer_time;
	Integer seller_time;
	String stock_name;
	Integer quantity;
	Double trade_price;
	
	public Trade()
	{
		this.buyer = "";
		this.seller = "";
		this.stock_name = "";
		this.buyer_time = -1;
		this.seller_time = -1;
		this.quantity = -1;
		this.trade_price = -1.0;		
	}
	
	//setter methods
	public void setBuyer(String value)
	{
		this.buyer = value;
	}
	public void setSeller(String value)
	{
		this.seller = value;
	}
	public void setStockName(String value)
	{
		this.stock_name = value;
	}
	public void setBuyerTime(Integer value)
	{
		this.buyer_time = value;
	}
	public void setSellerTime(Integer value)
	{
		this.seller_time = value;
	}
	public void setQuantity(Integer value)
	{
		this.quantity = value;
	}
	public void setPrice(Double value)
	{
		this.trade_price = value;
	}
			
			
			
	//getter methods
	public String getBuyer()
	{
		return this.buyer;
	}
	public String getSeller()
	{
		return this.seller;
	}
	public String getStockName()
	{
		return this.stock_name;
	}
	public Integer getBuyerTime()
	{
		return this.buyer_time;
	}
	public Integer getSellerTime()
	{
		return this.seller_time;
	}	
	public Integer getQuantity( )
	{
		return this.quantity;
	}
	public Double getTradePrice()
	{
		return this.trade_price;
	}
	
	
	
	
}
