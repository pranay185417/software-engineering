package stockmarket;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;

public class Operations 
{			
			
			static List<Trade> transactions = new ArrayList<Trade>();// list storing all pending orders
			
			// Reading and Validation of new Order
			//input : a line of the input csv file
			//output : a valid input order
			public Order validateInput(String line,String splitby)
			{	
				  boolean correct_input = true;
				  Order input = new Order();
				  
				  String[] values = line.split(splitby);
				  
				  Integer time = Integer.parseInt(values[0]);
				  String type = values[1];
				  String client = values[2];
				  String stock = values[3];
				  Integer quantity = Integer.parseInt(values[4]);
				  Double price = Double.parseDouble(values[5]);				  

				  
				  //checking for validating conditions:
				  //1. time > 0 and greater than time of previous order
				  //2. type must not be other than B or S
				  //3. quantity > 0
				  //4. price > 0
				  if((time < 0) || (time < Main.prev_time)  ||  (!(type.equals("S")) )   &&    (!(type.equals("B")))   || quantity <= 0 || price <= 0 )
				  {
					  correct_input = false;
					  System.out.println("invalid input");
				  }		
				  
				  if(correct_input == true)
				  {			  
					  input.setTime(time);
					  input.setType(type);
					  input.setClient(client);
					  input.setStock(stock);
					  input.setQuantity(quantity);
					  input.setPrice(price);					  
					  return input;
				  }
				  
				  return input;				  
			}
			
			
			
			
			//input : input order
			//adds the given input order to pending order list
			public void addToPending(Order input)
			{	
				Main.pending.add(input);
				Main.prev_time = input.getTime();
			}
			
			
			
			
			
			//input : input order
			//output : returns index of that order in pending list which matches with input order
			//matching criteria : (input.type != order.type) and (input.stock == order.stock ) and ( (input.type == "S" and input.price <= order.price) or (input.type == "B" and input.price >= order.price) )
			public int getMatchingOrder(Order input)
			{
				int id = -1;
				
				String input_type = input.getType();
				String input_stock = input.getStock();
				Double input_price = input.getPrice();	
				 
				
				for (Order order : Main.pending) //iterate over each order in pending list
				{
					String pending_order_type = order.getType();
					String pending_order_stock = order.getStock();
					Double pending_order_price = order.getPrice();
					
					//applying the matching criteria
					if(   (pending_order_type.equals(input_type)== false)   &&   (input_stock.equals(pending_order_stock) == true)  &&  (  ((input_type.equals("S"))  &&  (input_price <= pending_order_price))      ||     ((input_type.equals("B"))  &&  (input_price >= pending_order_price))   ))
					{
						id = Main.pending.indexOf(order);
						return id;									
					}				
			    }
				
				return id;
			}	
			
			
			
			
			//input : input order and index of matched order from pending list
			//output : returns an order of modified input order after doing trade with matched order in pending list
			// after doing trade function also updates the values of the matched order in pending list
			// records transaction details buyer,seller,buyer order time,seller order time,stock name,quantity,price
			// transaction price = seller price
			public Order doTrade(Order input , int id )
			{
				Trade transaction = new Trade();
				
				Integer input_time = input.getTime();
				Integer input_quantity = input.getQuantity();
				String input_client = input.getClient();
				String input_type = input.getType();
				Double input_price = input.getPrice();
				String input_stock = input.getStock();
				
				Order order = Main.pending.get(id);
				Integer pending_order_time = order.getTime();
				Integer pending_order_quantity = order.getQuantity();
				String pending_order_client = order.getClient();
				Double pending_order_price = order.getPrice();
				
				
				if(input_quantity > pending_order_quantity)
				{
					if(input_type.equals("B") )
					{
						transaction.setBuyer(input_client);
						transaction.setSeller(pending_order_client);
						transaction.setBuyerTime(input_time);
						transaction.setSellerTime(pending_order_time);
						transaction.setStockName(input_stock);
						transaction.setQuantity(pending_order_quantity);
						transaction.setPrice(pending_order_price);						
					}
					else
					{
						transaction.setBuyer(pending_order_client);
						transaction.setSeller(input_client);
						transaction.setBuyerTime(pending_order_time);
						transaction.setSellerTime(input_time);
						transaction.setStockName(input_stock);
						transaction.setQuantity(pending_order_quantity);
						transaction.setPrice(input_price);						
					}					
					Integer difference = input_quantity - pending_order_quantity ;//remaining quantity of input
					input.setQuantity(difference);//updating the input quantity value
					order.setQuantity(0); //changing the pending order quantity to 0	
					Main.pending.remove(id);
					Main.pending.add(id,order); // updating order to the pending list		
				}
				else 
				{
					if(input_type.equals("B") )
					{
						transaction.setBuyer(input_client);
						transaction.setSeller(pending_order_client);
						transaction.setBuyerTime(input_time);
						transaction.setSellerTime(pending_order_time);
						transaction.setStockName(input_stock);
						transaction.setQuantity(input_quantity);
						transaction.setPrice(pending_order_price);
						
					}
					else
					{
						transaction.setBuyer(pending_order_client);
						transaction.setSeller(input_client);
						transaction.setBuyerTime(pending_order_time);
						transaction.setSellerTime(input_time);
						transaction.setStockName(input_stock);
						transaction.setQuantity(input_quantity);
						transaction.setPrice(input_price);
					}			
					
					Integer difference = pending_order_quantity - input_quantity ;//remaining quantity of pending_order
					input.setQuantity(0);//changing the input quantity to 0
					order.setQuantity(difference);	//updating the pending order quantity value
					Main.pending.remove(id);
					Main.pending.add(id,order);// updating order to the pending list	
					
				}
				transactions.add(transaction);
				return input;
			}		
			
			
			
			
			
			//PRINTING FUNCTIONS
			
			//takes no input and prints contents of pending list in tabular form			
			public void printPending()  
		    { 
		        if (Main.pending.isEmpty())  
		        { 
		            System.out.println("pending is empty");
		            System.out.println("|________________________________PENDING ORDERS_______________________________|");
		            System.out.println("|time	 |	type	 |	client	 |	stock	 |	quantity	 |	price |");
		            System.out.println("--------------------------------------------------------------------------------");
		            System.out.println("|_____________________________________________________________________________|");
		        } 
		          
		        else
		        { 
		        	System.out.println("|________________________________PENDING ORDERS_______________________________|");
		        	System.out.println("|time    |  type     |  client     |	stock    |	quantity     |	price |");
		        	System.out.println("--------------------------------------------------------------------------------");
		        	for (Order order : Main.pending) {
		                System.out.println("|"+order.getTime()+"	 |	"+order.getType()+"    |   "+order.getClient()+"	   |  "+order.getStock()+"	 |	"+order.getQuantity()+"	     | "+order.getPrice()+"  |");
		            } 
		        	System.out.println("|_____________________________________________________________________________|");
		        } 
		    }
			
			
			//takes no input and prints trades
			public void printTrades()
			{
				  if(transactions.isEmpty())
				  {
					  System.out.println("no transactions");					  
				  }
				  else
				  {
					  for (Trade transaction : transactions)
					  {
						  System.out.println(transaction.getBuyer()+" who arrived at time : "+transaction.getBuyerTime()+" "+" bought "+transaction.getQuantity()+" of "+transaction.getStockName()+" shares from "+transaction.getSeller()+" who arrived at time : "+transaction.getSellerTime()+" at a cost of "+transaction.getTradePrice()+" per unit");
						  
					  }
				  }
			}
			
			
			//takes no input and prints final Output
			public void printOutput()
			{
				System.out.println("Trades :");
				this.printTrades();
				System.out.println("");
				this.printPending();				  
			}	
			
			
			
}
