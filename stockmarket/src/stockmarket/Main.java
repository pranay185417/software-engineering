package stockmarket;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main
{
	static List<Order> pending = new ArrayList<Order>();// list storing all pending orders
	static int prev_time = 0;// time of the last input order added to pending list
	static String csv = "";	
	static String splitby = ",";//csv file separator
	
	public static void main(String [] args)
	{		  
		  //ASSUMPTION: csv file is given in command line		  
		  csv = "C:\\Users\\input.csv";
		  String line = "";
		  Operations operation = new Operations();
		  
		  
		  try(BufferedReader br = new BufferedReader(new FileReader(csv)))
		  {
			  while((line = br.readLine()) != null)
			  {	  
				  // while condition : reads a line from csv file and checks that it is not null
				  //breaks when line is null
				  
				  //SUBTASK 1 : Reading and Validation of New Input Order
				  
				  Order input = new Order();
				  
				  input = operation.validateInput(line,splitby);	
				  
				  if(input.isEmpty())
				  {
					  continue;
				  }
				  
				  
				  
				  
				  // SUBTASK 2 : Finding (zero/more) matching orders and trading
				  
				  //first check for empty pending list
				  if(pending.isEmpty() == true)
				  {
					  operation.addToPending(input);
					  continue;//going for next order
				  }
				  else
				  {		

					  while( input.getQuantity() != 0) //until input quantity equals 0
					  { 
						  //if this while loop breaks then program takes next input order						  

						  //SUBTASK 2.1 : Produce matching trade
						  int id = operation.getMatchingOrder(input);
						  
						  // If no matching order found then add to pending and repeat
						  if(id < 0) 
						  {
							  operation.addToPending(input);
							  break;
						  }
						  
						  //If match found then trade
						  input = operation.doTrade(input,id);
						  
						  //After trade
						  Integer matched_stock_quantity = pending.get(id).getQuantity();					  
						  
						  //SUBTASK 2.2 : Updating pending orders
						  if(matched_stock_quantity == 0) //also matched stock quantity becomes 0 then remove it from pending
						  {							  
							  pending.remove(id);							 
						  }					  
						  
						  
					  }					  
				  }
				  
				  
			  }			  
			  
		  }
		
		  catch(IOException e)
		  {
			  e.printStackTrace();
		  }		  
		  
		  // SUBTASK 3 : Output trades to customers
		  operation.printOutput();		  
		 
	}
}




