package stockmarket;

public class Order 
{
		Integer time;
		String type;
		String client;
		String stock;
		Integer quantity;
		Double price;
		
		
		
		//constructor		
		public Order()
		{
			this.time = -1 ;
			this.type = "";
			this.client = "";
			this.stock = "";
			this.quantity = -1;
			this.price = -1.0;			
		}
		
		//setter methods
		public void setTime(Integer value)
		{
			this.time = value;
		}
		public void setType(String value)
		{
			this.type = value;
		}
		public void setClient(String value)
		{
			this.client = value;
		}
		public void setStock(String value)
		{
			this.stock = value;
		}
		public void setQuantity(Integer value)
		{
			this.quantity = value;
		}
		public void setPrice(Double value)
		{
			this.price = value;
		}
		
		
		
		//getter methods
		public Integer getTime()
		{
			return this.time;
		}
		public String getType()
		{
			return this.type;
		}
		public String getClient()
		{
			return this.client;
		}
		public String  getStock()
		{
			return this.stock;
		}
		public Integer getQuantity( )
		{
			return this.quantity;
		}
		public Double getPrice()
		{
			return this.price;
		}
		
		
		
		public boolean isEmpty()
		{
			if(this.time == -1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		
}
