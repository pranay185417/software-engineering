package stockmarket;

import org.junit.Test;

public class ValidateInputTest {

	@Test
	public void validateInputTest() 
	{
		Operations operation = new Operations();
		
		Order test = new Order();
		String line = "1,S,C1,S1,50,150.0";	
		
		Order expected = new Order();
		expected.setTime(1);
		expected.setType("S");
		expected.setClient("C1");
		expected.setStock("S1");
		expected.setQuantity(50);
		expected.setPrice(150.0);
		
		test = operation.validateInput(line, Main.splitby);	

		assert(expected.equals(test));
		
		
	}

}
