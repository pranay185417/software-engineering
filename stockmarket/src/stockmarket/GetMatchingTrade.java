package stockmarket;

import static org.junit.Assert.*;

import org.junit.Test;

public class GetMatchingTrade {	
	
	@Test
	public void getMatchingIdTest()
	{
		Main.pending.clear();
		Main.prev_time = 0 ;
		
		Operations operation = new Operations();
		
		Order order1 = new Order();				
		order1.setTime(1);
		order1.setType("S");
		order1.setClient("C1");
		order1.setStock("S1");
		order1.setQuantity(10);
		order1.setPrice(100.0);
		

		Order order2 = new Order();
		order2.setTime(2);
		order2.setType("S");
		order2.setClient("C2");
		order2.setStock("S2");
		order2.setQuantity(10);
		order2.setPrice(100.0);
		

		Order input = new Order();
		input.setTime(3);
		input.setType("B");
		input.setClient("C3");
		input.setStock("S2");
		input.setQuantity(11);
		input.setPrice(200.0);		

		operation.addToPending(order1);
		operation.addToPending(order2);

		int expected = 1;

		int id = operation.getMatchingOrder(input);

		assertEquals(id,expected);
	}


}
